<?php


namespace App;

use App\JsonResourceException;

class JsonResource implements ResourceDater
{
    private string $filepath;
    private array $jsonData;

    public function __construct($filepath)
    {
        $this->setJsonFile($filepath);
        $this->saveData();
    }

    private function setJsonFile($filepath): void
    {
        if(preg_match('/^(.*)\.json$/', $filepath) && file_exists($filepath)) {
            $this->filepath = $filepath;
        } else {
            throw new JsonResourceException("File {$filepath} is not exist or not int .json format.");
        }
    }
    private function saveData(): void
    {
        $strJsonFileContents = file_get_contents($this->filepath);
        // Convert to array
        $this->jsonData = json_decode($strJsonFileContents, true);
    }

    public function getData(): array
    {
        return $this->jsonData ?? array();
    }
}