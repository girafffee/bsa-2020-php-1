<?php

namespace App;


abstract class HtmlPresenter
{
    private const PATH_TO_TEMPLATES = __DIR__ . '/Task3/templates/';

    public function render(array $data, string $tplName): string
    {
        extract($data);

        ob_start();
        require self::PATH_TO_TEMPLATES . $tplName;

        $html = ob_get_contents();
        ob_clean();

        return $html;
    }

    abstract public function present(): string;

}