<?php

declare(strict_types=1);

namespace App;

interface ResourceDater
{
    public function getData(): array;
}