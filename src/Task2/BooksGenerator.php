<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private int $minPagesNumber;
    private array $libraryBooks;
    private int $maxPrice;
    private array $storeBooks;

    public function __construct(
        int $minPagesNumber,
        array $libraryBooks,
        int $maxPrice,
        array $storeBooks
    )
    {
        $this->minPagesNumber   = $minPagesNumber;
        $this->libraryBooks     = $libraryBooks;
        $this->maxPrice         = $maxPrice;
        $this->storeBooks       = $storeBooks;
    }

    private function filterLibraryBooks(): \Generator
    {
        foreach ($this->libraryBooks as $libBook) {
            if ($libBook->getPagesNumber() >= $this->minPagesNumber){
                yield $libBook;
            }
        }
    }

    private function filterStoreBooks(): \Generator
    {
        foreach ($this->storeBooks as $storeBook) {
            if ($storeBook->getPrice() <= $this->maxPrice){
                yield $storeBook;
            }
        }
    }

    public function generate(): \Generator
    {
        yield from $this->filterLibraryBooks();
        yield from $this->filterStoreBooks();
    }
}