<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;
use App\HtmlPresenter;


class CarTrackHtmlPresenter extends HtmlPresenter
{
    private Track $trackObj;

    public function __construct(Track $trackObj)
    {
        $this->setObject($trackObj);
    }

    protected function setObject(Track $trackObj): void
    {
        $this->trackObj = $trackObj;
    }

    private function getCarInfo(Car $car): array
    {
        return [
            'path'  => $car->getImage(),
            'name'  => $car->getName(),
            'id'    => $car->getId(),
            'speed' => $car->getSpeed()
        ];
    }

    public function present(): string
    {
        $html = "";
        foreach ($this->trackObj->all() as $car) {

            $html .= $this->render(
                $this->getCarInfo($car),
                'car.tpl.php'
            );
        }
        return $html;
    }

    public function presentResults(): string
    {
        $cars = $this->trackObj->all();
        usort($cars, function ($prev, $next)
        {
            return $this->trackObj->getFinishCar($prev) <=> $this->trackObj->getFinishCar($next);
        });


        return $this->render([
            'cars'  => $cars,
            'track' => $this->trackObj
        ],'race.results.tpl.php');

    }
}