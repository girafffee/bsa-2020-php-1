<?php


namespace App\Task3;


class ExceptionPresenter extends \App\HtmlPresenter
{
    private \Throwable $exception;

    public function __construct(\Throwable $exception)
    {
        $this->exception = $exception;
    }

    public function present(): string
    {
        return $this->render(['exception' => $this->exception], 'exception.tpl.php');
    }
}