<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\Track;
use App\Task3\{ExceptionPresenter, CarTrackHtmlPresenter, CarWinnerHtmlPresenter, TrackException};
use App\{JsonResourceException, JsonResource};

$exception = "";

try {
    $arena = (new Track(40, 40))
    ->addCars(new JsonResource('data.cars.json'));

    $viewRace = new CarTrackHtmlPresenter($arena);

    $winner = $arena->run();

    $presentation = $viewRace->present();
    $tableResults = $viewRace->presentResults();
    $winnerHtml = (new CarWinnerHtmlPresenter($winner, $arena))->present();

} catch (TrackException|JsonResourceException $exception) {
    $exception = (new ExceptionPresenter($exception))->present();
}

require_once 'front-page.php';
