<div class="card" style="width: 18rem; margin: 1rem auto;">
    <img src="<?=$path?>" alt="winner car">
    <div class="card-body">
        <h3>Winner</h3>
        <p class="card-text"><?=$name?>:  finished in <?=$finishTime?> minutes</p>
    </div>
</div>