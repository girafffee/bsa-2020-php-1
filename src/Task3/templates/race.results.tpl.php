<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">Name</th>
        <th scope="col">Finished in (min)</th>
        <th scope="col">Speed</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($cars as $car): ?>
        <tr>
            <th scope="row"><?=$car->getId();?></th>
            <td><?=$car->getName();?></td>
            <td><?=$track->getFinishCar($car);?></td>
            <td><?=$car->getSpeed();?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
