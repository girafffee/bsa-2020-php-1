<?php


namespace App\Task3;

use App\HtmlPresenter;
use App\Task1\Car;
use App\Task1\Track;

class CarWinnerHtmlPresenter extends HtmlPresenter
{
    private Car $carObj;
    private Track $trackObj;

    public function __construct(Car $car, Track $track)
    {
        $this->carObj = $car;
        $this->trackObj = $track;
    }

    private function getCarInfo(): array
    {
        return [
            'path'  => $this->carObj->getImage(),
            'name'  => $this->carObj->getName(),
            'finishTime' => $this->trackObj->getFinishCar($this->carObj)
        ];
    }

    public function present(): string
    {
        return $this->render($this->getCarInfo(),'car.winner.tpl.php');
    }
}