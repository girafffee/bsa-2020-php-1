<?php

declare(strict_types=1);

namespace App\Task1;

use App\ResourceDater;
use App\Task3\TrackException;

class Track
{
    // track length, in km
    private float $lapLength;

    // number of laps
    private int $lapsNumber;

    private float $trackLength;
    private array $carsInTrack;

    /*
     * Contains an array with the results
     * of finishing machines
     */
    private array $resultsRace;

    private Car $winner;


    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
        $this->setTrackLength();
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->carsInTrack[] = $car;
    }

    public function all(): array
    {
        return $this->carsInTrack;
    }

    /**
     * Get the total race length
     * @return float
     */
    private function getTrackLength(): float
    {
        return $this->trackLength;
    }


    /**
     * Set the total race length
     */
    private function setTrackLength(): void
    {
        $this->trackLength = $this->lapsNumber * $this->lapLength;
    }

    /**
     * Get the minimum number of minutes this car can drive
     * @param Car $car
     * @return float
     */
    private function getAllLengthTime(Car $car): float
    {
        /*
         * Multiply by 60, (translate hours to minutes)
         * to add them to the minutes at the pit stop
         * in $this->getFinishTime()
         */
        return (($this->getTrackLength() / $car->getSpeed()) * 60);
    }

    /**
     * How much fuel the car consumes for the entire race
     * @param Car $car
     * @return float
     */
    private function getRaceConsumption(Car $car): float
    {
        return ($this->getTrackLength() / 100 * $car->getFuelConsumption());
    }

    /**
     * How many times will the car stop at the pit stop
     * @param Car $car
     * @return int
     */
    private function countRefills(Car $car): int
    {
        /*
         * Here we lead to an integer, because initially
         * the car starts with a full tank.
         */
        return (int)($this->getRaceConsumption($car) / $car->getFuelTankVolume());
    }

    /**
     * All the time spent on pit stops
     * @param Car $car
     * @return int
     */
    private function getTimeForPitStops(Car $car): int
    {
        return $this->countRefills($car) * $car->getPitStopTime();
    }

    /**
     * Time for which the car will finish
     * @param Car $car
     * @return float
     */
    private function finish(Car $car): float
    {
        $finishTime = $this->getTimeForPitStops($car) + $this->getAllLengthTime($car);
        $this->setFinishCar($car, $finishTime);

        return $this->getFinishCar($car);
    }

    public function getFinishCar(Car $car): float
    {
        return (float) sprintf("%.2f", $this->resultsRace[$car->getId()]);
    }

    private function setFinishCar(Car $car, float $finishTime): void
    {
        if(in_array($car, $this->carsInTrack))
            $this->resultsRace[$car->getId()] = $finishTime;
    }

    public function run(): Car
    {
        $this->beforeRun();
        $winner = $this->carsInTrack[0];
        $minTime = $this->finish($winner);

        foreach ($this->all() as $car) {
            $this->finish($car);

            if ($this->getFinishCar($car) < $minTime) {
                $minTime = $this->getFinishCar($car);
                $winner = $car;
            }
        }
        $this->winner = $winner;

        return $winner;
    }

    private function beforeRun(): void
    {
        if(!$this->hasCarsInTrack())
            throw new TrackException("Not enough cars in the race" );
    }

    private function hasCarsInTrack($min = 2): bool
    {
        $size = sizeof($this->carsInTrack);
        return ($size >= $min && $size > 0);
    }

    public function getWinner()
    {
        if (is_a($this->winner, Car::class)){
            return $this->winner;
        } else {
            return false;
        }

    }

    public function addCars(ResourceDater $resource): Track
    {
        foreach ($resource->getData() as $object) {
            $this->add(new Car(
                $object['id'],
                $object['image'],
                $object['name'],
                $object['speed'],
                $object['pitStopTime'],
                $object['fuelConsumption'],
                $object['fuelTankVolume']
            ));
        }
        return $this;
    }
}